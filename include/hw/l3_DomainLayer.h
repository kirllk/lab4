#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"

const int MAX_NAME = 50;
const int MAX_SURNAME = 50;
const int MAX_LASTNAME = 50;
const int MIN_ID = 1;
const int MIN_YEAR = 1900;
const int MAX_YEAR = 2300;
const int MIN_RATING = 0;
const int MAX_RATING = 100;
const int MAX_KAF = 10;

enum AcademicDegree {
  assistant_professor,
  professor,
  senior_lecturer,
  teacher,
  graduate_student,
  master,
  bachelor
};

class ElectronicJournal : public ICollectable {
 protected:
  bool invariant() const;
 public:
  ElectronicJournal() = delete;
  ElectronicJournal(const ElectronicJournal& p) = delete;
  ElectronicJournal& operator=(const ElectronicJournal& p) = delete;
  ElectronicJournal(std::string name, std::string surname, std::string lastname, int id, int year, std::string kaf, AcademicDegree academicDegree, int rating, int procentPortfolio);
  std::string getFIO() const;
  int getId() const;
  int getYear() const;
  int getRating() const;
  const std::string& getKaf() const;
  std::string getAcademicDegree() const;
  bool write(std::ostream& os) override;
  int getPortfolio() const;
 private:
  std::string _name;
  std::string _surname;
  std::string _lastname;
  int _id;
  int _year;
  std::string _kaf;
  AcademicDegree _academicDegree;
  int _rating;
  int _procentPortfolio;
};

class Curator {
 public:
  Curator()=delete;
  explicit Curator(std::string name);
  void addStudent(const std::shared_ptr<ICollectable>& student);
  int countStudents() const;
  int getMiddle() const;
  std::string getName() const;
  bool operator<( const Curator& val ) const {
	return getMiddle() > val.getMiddle();
  }
 private:
  std::vector<std::shared_ptr<ICollectable>> _students;
  std::string _name;
};

struct CuratorGreater {
  bool operator()( const Curator& lx, const Curator& rx ) const {
	return lx < rx;
  }
};

class ItemCollector: public ACollector {
  std::vector<Curator> _curators;
 public:
  void addCurator(const Curator& curator);
  std::vector<Curator> getCurators() const;
  std::vector<Curator> getSortedCurators();
  void  addNewStudent(int index_curator, int index);
  void readC(std::istream& is) override ;
  std::shared_ptr<ICollectable> read(std::istream& is) override;
  bool writeC(std::ostream& os) const override;
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
